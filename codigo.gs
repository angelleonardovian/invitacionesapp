var ss = SpreadsheetApp.getActiveSpreadsheet();  
var ui = SpreadsheetApp.getUi();


function onOpen(e) {  
  var menu = ui.createMenu('InvitacionesApp');
  menu.addItem('Origen datos', 'option1');
  menu.addItem('Mensaje Invitación', 'option2');
  menu.addItem('Enviar invitación', 'option3');
  menu.addToUi();
}

function option1(){  
  showDatosForm();
}

function option2(){  
  showMensajeForm();
}

function option3(){  
  showInvitacionForm();
}

//************ INICIO CREACION HTML ************
function showDatosForm(){
  var template = HtmlService.createTemplateFromFile("DatosForm");
  
  var html = template.evaluate();
  html.setTitle("Origen de los datos");
  SpreadsheetApp.getUi().showSidebar(html);  

}

function showMensajeForm(){
  var template = HtmlService.createTemplateFromFile("MensajeForm");
  
  var html = template.evaluate();
  html.setTitle("Mensaje");
  SpreadsheetApp.getUi().showSidebar(html);

}

function showInvitacionForm(){
  var template = HtmlService.createTemplateFromFile("InvitacionForm");
  
  var html = template.evaluate();
  html.setTitle("Envío de Invitación");
  SpreadsheetApp.getUi().showSidebar(html);

}
//************ FIN CREACION HTML ************


//************ INICIO FUNCIONES DATOS ************
function getData(){  
  var sp = SpreadsheetApp.getActive().getSheetByName("Parametros");
  var data = {
    C0: "",
    C1: "",
    M0: "",
    M1: "",
    L0: "",
    L1: ""
  };
  
  if(sp == null){
    return data;
  }
  else{
    var valores = sp.getRange(1,2,6).getValues();
    data.C0 = valores[0];
    data.C1 = valores[1];
    data.M0 = valores[2];
    data.M1 = valores[3];
    data.L0 = valores[4];
    data.L1 = valores[5];
    
    return data;
  }  
}

function getDataMsg(){  
  var ss = SpreadsheetApp.getActive().getSheetByName("Parametros");
  var data = "";
  
  if(ss != null){
    data = ss.getRange(1,4).getValue();
  }    
  
  return data;    
}

function setData(dataHtml){    
  var sheet = makeSheet();
  writeData(sheet,dataHtml); 
}

function setDataMsg(dataHtml){    
  var sheet = makeSheet();
  writeDataMsg(sheet,dataHtml); 
}

function makeSheet(){
  var ww = SpreadsheetApp.getActive(); 
  var sheetActive = SpreadsheetApp.getActive().getActiveSheet();
  var sheet = ww.getSheetByName("Parametros");
  
  if(sheet == null){
    sheet = ww.insertSheet("Parametros");  
    sheet.hideSheet();
  }
  
  ww.setActiveSheet(sheetActive); 
  return sheet;
}

function writeData(sheet, dataHtml){  
  var col1 = [["RangoCliente0"],["RangoCliente1"],["RangoMail0"],["RangoMail1"],["RangoLeng0"],["RangoLeng1"]];
  var col2 = [[dataHtml.C0],[dataHtml.C1],[dataHtml.M0],[dataHtml.M1],[dataHtml.L0],[dataHtml.L1]];  

  sheet.getRange(1, 1, 6).setValues(col1);
  sheet.getRange(1, 2, 6).setValues(col2);
}

function writeDataMsg(sheet, dataMsgHtml){
  sheet.getRange(1, 3).setValue("Mensaje de invitacion");
  sheet.getRange(1, 4).setValue(dataMsgHtml);
}
//************ FIN FUNCIONES DATOS ************


function crearEvento(video, date, time1, time2, remitente){
  ui.alert(video+"//"+date+"//"+time1+"//"+time2+"//"+remitente);
}

function calendarizar(title, video, date, time1, time2){
  
  //Tomar todos los datos
  var p = getData();
  var nombres = ss.getRange(p.C0+":"+p.C1).getValues();
  var emails = ss.getRange(p.M0+":"+p.M1).getValues();  
  var leng = ss.getRange(p.L0+":"+p.L1).getValues();  
  
    //datos del creador del evento   
    var userId = Session.getActiveUser().getEmail();
    var userCalendar= CalendarApp.getCalendarById(userId);    
  
    //Creacion de evento
    var starTime = new Date(date +" " + time1 + ":00 EST");
    var endTime = new Date(date +" " + time2 + ":00 EST");
    var evento = userCalendar.createEvent(title, starTime, endTime, {sendInvites: true});     
    evento.setDescription(getDataMsg());
    evento.setColor("10");  
  
    for(i=0; i < emails.length; i++){
       evento.addGuest(emails[i]);
       sendEmail(nombres[i], emails[i], leng[i], evento);
    }
  
    evento.setGuestsCanSeeGuests(false);//Que los invitados no puedan ver los demás invitados
    ui.alert("Invitacion enviada exitosamente!");    
}

  function sendEmail(nombre, email, leng, evento){ 
    
    var mensaje = "Hola, "+ nombre + "\n" +
                     evento.getDescription() + " \n\n" +
                     "Creador del evento: "+ evento.getCreators() + " \n\n" +
                     "Fecha Inicio del evento: " + evento.getStartTime()  + " \n\n" +
                     "Fecha Fin del evento: " + evento.getEndTime() + " \n\n" +
                     "Puede consultar su Calendario de Google y autorizar la asistencia al evento.";
    
    if(leng == "EN" || leng == "en" || leng == "ingles"){
       mensaje = traslate(mensaje);
    }  
    
    GmailApp.sendEmail(email, evento.getTitle(), mensaje);     
  }


function traslate(msg){  
  var menTraducido = LanguageApp.translate(msg , 'es', 'en');
  return menTraducido;
}



